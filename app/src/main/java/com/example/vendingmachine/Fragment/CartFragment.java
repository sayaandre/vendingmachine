package com.example.vendingmachine.Fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vendingmachine.Activity.AddItemActivity;
import com.example.vendingmachine.Adapter.CartAdapter;
import com.example.vendingmachine.Adapter.MainAdapter;
import com.example.vendingmachine.Helper.CartHelper;
import com.example.vendingmachine.Helper.ItemMasterHelper;
import com.example.vendingmachine.Helper.Tools;
import com.example.vendingmachine.Models.Model.CartModel;
import com.example.vendingmachine.Models.Model.ItemMasterModel;
import com.example.vendingmachine.Models.Realm.Cart;
import com.example.vendingmachine.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmResults;

public class CartFragment extends Fragment {

    @BindView(R.id.btnVoid) Button btnVoid;
    @BindView(R.id.rvCart) RecyclerView rvItem;
    @BindView(R.id.txtTotal) TextView txtTotal;
    @BindView(R.id.btnPay) Button btnPay;
    @BindView(R.id.txt2000) TextView txt2000;
    @BindView(R.id.txt5000) TextView txt5000;
    @BindView(R.id.txt10000) TextView txt10000;
    @BindView(R.id.txt20000) TextView txt20000;
    @BindView(R.id.txt50000) TextView txt50000;

    Unbinder unbinder;
    Realm realm;
    private CartAdapter adapter;
    List<CartModel> cartModels;
    CartHelper helper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
        helper = new CartHelper(getActivity());
        cartModels = new ArrayList<>();
        setHasOptionsMenu(true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_cart, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        rvItem.setLayoutManager(mLayoutManager);
        rvItem.addItemDecoration(new GridSpacingItemDecoration(1, 0, true));
        rvItem.setItemAnimator(new DefaultItemAnimator());
        setAdapter();
        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double totalBayar = (Integer.parseInt(txt2000.getText().toString().isEmpty() ? "0" : txt2000.getText().toString()) * 2000) +
                        (Integer.parseInt(txt5000.getText().toString().isEmpty() ? "0" : txt5000.getText().toString()) * 5000) +
                        (Integer.parseInt(txt10000.getText().toString().isEmpty() ? "0" : txt10000.getText().toString()) * 10000) +
                        (Integer.parseInt(txt20000.getText().toString().isEmpty() ? "0" : txt20000.getText().toString()) * 20000) +
                        (Integer.parseInt(txt50000.getText().toString().isEmpty() ? "0" : txt50000.getText().toString()) * 50000) -
                        helper.getTotalInCart();
                if(totalBayar >= 0)
                {
                    realm.beginTransaction();
                    RealmResults<Cart> carts = realm.where(Cart.class).findAll();
                    carts.deleteAllFromRealm();
                    realm.commitTransaction();
                    setAdapter();
                    Tools.createAlertDialog(getActivity(), totalBayar > 0 ? "Pembayaran selesai dengan kembalian Rp. " +
                            Tools.convertMoney(getActivity(), totalBayar) + ", silahkan ditunggu" : "Pembayaran selesai, silahkan ditunggu..");
                    txt2000.setText("");
                    txt5000.setText("");
                    txt10000.setText("");
                    txt20000.setText("");
                    txt50000.setText("");
                }
                else if(totalBayar < 0)
                {
                   Tools.createAlertDialog(getActivity(), "Silahkan menyelesaikan pembayaran terlebih dahulu..");
                }
            }
        });
        btnVoid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder kotakBuilder = new AlertDialog.Builder(getActivity(),
                        R.style.AppCompatAlertDialogStyle);
                kotakBuilder.setIcon(getResources().getDrawable(R.drawable.ic_shortcut_baseline_report_problem_black_48));
                kotakBuilder.setTitle("Notice");
                kotakBuilder.setMessage("Apakah anda ingin menghapus semua barang di keranjang?");
                kotakBuilder.setPositiveButton("Ya",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                realm.beginTransaction();
                                RealmResults<Cart> carts = realm.where(Cart.class).findAll();
                                carts.deleteAllFromRealm();
                                realm.commitTransaction();
                                setAdapter();
                            }
                        });
                kotakBuilder.setNegativeButton("Batalkan", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = kotakBuilder.create();
                dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_white));
                dialog.show();
            }
        });
        return rootView;
    }

    private void setAdapter()
    {
        cartModels.clear();
        cartModels.addAll(helper.getCart());
        txtTotal.setText("Rp. " + Tools.convertMoney(getActivity(), helper.getTotalInCart()));
        if(adapter == null) {
            adapter = new CartAdapter(getActivity(), cartModels, new CartAdapter.OnItemClickListener() {
                @Override
                public void onClick(CartModel item) {

                }

                @Override
                public void onDeleteClick(CartModel item) {

                }
            });
            rvItem.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK)
        {
            if (requestCode == 100)
            {
                setAdapter();
            }
            else if(requestCode == 200)
            {
                setAdapter();
            }
        }
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }
}
