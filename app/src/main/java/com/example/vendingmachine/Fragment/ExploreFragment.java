package com.example.vendingmachine.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vendingmachine.Activity.AddItemActivity;
import com.example.vendingmachine.Adapter.MainAdapter;
import com.example.vendingmachine.Helper.ItemMasterHelper;
import com.example.vendingmachine.Models.Model.ItemMasterModel;
import com.example.vendingmachine.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;

public class ExploreFragment extends Fragment {

    @BindView(R.id.rvItemMaster) RecyclerView rvItem;
    @BindView(R.id.txtDataFound) TextView txtDataFound;

    Unbinder unbinder;
    Realm realm;
    private MainAdapter adapter;
    List<ItemMasterModel> itemMasterModelList;
    List<ItemMasterModel> itemMasterModelListFull;
    ItemMasterHelper helper;
    boolean tabletSize = false;
    MenuItem menuitem;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
        helper = new ItemMasterHelper();
        itemMasterModelList = new ArrayList<>();
        itemMasterModelListFull = new ArrayList<>();
        setHasOptionsMenu(true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_explore, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        rvItem.setLayoutManager(mLayoutManager);
        rvItem.addItemDecoration(new GridSpacingItemDecoration(1, 0, true));
        rvItem.setItemAnimator(new DefaultItemAnimator());
        setAdapter();
        return rootView;
    }

    private void setAdapter()
    {
        itemMasterModelList.clear();
        itemMasterModelList.addAll(helper.getItemMaster());
//        if(mode == 1)
//        {
//            itemMasterModelListFull.clear();
//            itemMasterModelListFull.addAll(helper.getItemMasterFull());
//        }
        if(itemMasterModelList.size() > 0)
        {
            txtDataFound.setVisibility(View.GONE);
        }
        else
        {
            txtDataFound.setVisibility(View.VISIBLE);
        }
        if(adapter == null) {
            adapter = new MainAdapter(getActivity(), itemMasterModelList, new MainAdapter.OnItemClickListener() {
                @Override
                public void onClick(ItemMasterModel item, int position) {
                    if(item.getItemStok() <= 0)
                    {
                        Toast.makeText(getActivity(), "Insufficient stock", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Intent i = new Intent(getActivity(), AddItemActivity.class);
                        i.putExtra("item_id", item.getItemId());
                        startActivityForResult(i, 100);
                    }
                }
            });
            rvItem.setAdapter(adapter);
        } else {
            adapter.updateData(itemMasterModelList);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK)
        {
            if (requestCode == 100)
            {
                setAdapter();
            }
            else if(requestCode == 200)
            {
                setAdapter();
            }
        }
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }
}
