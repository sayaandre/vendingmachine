package com.example.vendingmachine.Activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vendingmachine.Helper.CartHelper;
import com.example.vendingmachine.Helper.CircleTransform;
import com.example.vendingmachine.Helper.Tools;
import com.example.vendingmachine.Models.Realm.Cart;
import com.example.vendingmachine.Models.Realm.ItemMaster;
import com.example.vendingmachine.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmResults;
import io.realm.Sort;

public class AddItemActivity extends BaseActivity {

    @BindView(R.id.imgItem) ImageView imgItem;
    @BindView(R.id.txtPCName) TextView txtPCName;
    @BindView(R.id.txtStock) TextView txtStock;
    @BindView(R.id.txtPCPrice) TextView txtPCPrice;
    @BindView(R.id.llPCDesc) LinearLayout llPCDesc;
    @BindView(R.id.txtPCDescTitle) TextView txtPCDescTitle;
    @BindView(R.id.txtPCDesc) TextView txtPCDesc;
    @BindView(R.id.btnMinus) Button btnMinus;
    @BindView(R.id.editQty) EditText txtQty;
    @BindView(R.id.btnPlus) Button btnPlus;
    @BindView(R.id.txtTotal) TextView txtTotal;
    @BindView(R.id.btnBuy) Button btnBuy;

    Bundle bundle;
    ItemMaster itemMaster;
    int quantity = 1;
    double price = 0.0;
    CartHelper helper;
    private String[] kodeTax, namaTax;
    private double[] rateTax;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        ButterKnife.bind(this);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        setTitle("Add Sales Order Item");
        helper = new CartHelper(this);
        bundle = getIntent().getExtras();
        if(bundle.containsKey("item_id"))
        {
            itemMaster = realm.where(ItemMaster.class).equalTo("itemId", bundle.getInt("item_id", 0)).findFirst();
            if(itemMaster != null)
            {
                if(itemMaster.getItemImage() == null || (itemMaster.getItemImage() != null && itemMaster.getItemImage().equals("")))
                {

                }
                else
                {
                    Picasso.with(this)
                            .load(itemMaster.getItemImage())
                            .centerCrop()
                            .transform(new CircleTransform(50,0))
                            .fit()
                            .into(imgItem);
                }
                Cart cart = realm.where(Cart.class)
                        .equalTo("itemId", bundle.getInt("item_id", 0))
                        .findFirst();
                if(cart != null)
                {
                    quantity = cart.getItemQty();
                    txtQty.setText(String.valueOf(quantity));
                    txtTotal.setText(Tools.convertMoney(this, cart.getItemPrice() * Double.parseDouble(txtQty.getText().toString())));
                }
                else
                {
                    txtTotal.setText(Tools.convertMoney(this, bundle.getDouble("item_price") * Double.parseDouble(txtQty.getText().toString())));
                }
                txtPCPrice.setText(Tools.convertMoney(this, itemMaster.getItemPrice()));
                txtStock.setText(itemMaster.getItemStok() + "");
                txtPCDesc.setText(itemMaster.getItemName().equalsIgnoreCase("null") ? "" : itemMaster.getItemName());
            }
            txtQty.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    int pos = txtQty.getText().length();
                    txtQty.setSelection(pos);
                    if(txtQty.getText().toString().length() >= 7)
                    {
                        txtQty.setText("999999");
                        Double total = Double.parseDouble(txtQty.getText().toString().equals("") ? "1" : txtQty.getText().toString()) * price;
                        txtTotal.setText(Tools.convertMoney(AddItemActivity.this, total));
                    }
                    else
                    {
                        if(txtQty.getText().toString().equals("0"))
                        {
                            txtQty.setText("1");
                            Double total = Double.parseDouble(txtQty.getText().toString().equals("") ? "1" : txtQty.getText().toString()) * price;
                            txtTotal.setText(Tools.convertMoney(AddItemActivity.this, total));
                        }
                        else
                        {
                            Double total = Double.parseDouble(txtQty.getText().toString().equals("") ? "1" : txtQty.getText().toString()) * price;
                            txtTotal.setText(Tools.convertMoney(AddItemActivity.this, total));
                        }
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
            btnPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Tools.dismissKeyboard(AddItemActivity.this);
                    if(!txtQty.getText().toString().isEmpty()) {
                        quantity = Integer.valueOf(txtQty.getText().toString());
                        quantity += 1;
                        if(String.valueOf(quantity).length() >= 7) {
                            quantity = 999999;
                        }
                    } else {
                        quantity = 1;
                    }
                    txtQty.setText(String.valueOf(quantity));
                }
            });
            btnMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Tools.dismissKeyboard(AddItemActivity.this);
                    if(!txtQty.getText().toString().isEmpty()) {
                        quantity = Integer.valueOf(txtQty.getText().toString());
                        if (quantity > 1)
                            quantity -= 1;
                    } else {
                        quantity = 1;
                    }
                    txtQty.setText(String.valueOf(quantity));
                }
            });
            btnBuy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Tools.dismissKeyboard(AddItemActivity.this);
                    if(itemMaster.getItemStok() >= Integer.parseInt(txtQty.getText().toString()))
                    {
                        helper.addCart(itemMaster.getItemId(), itemMaster.getItemName(), Integer.parseInt(txtQty.getText().toString()),
                                Tools.processMoney(txtPCPrice.getText().toString()), itemMaster.getItemImage());
                        Intent intent = new Intent();
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                    }
                    else
                    {
                        Toast.makeText(AddItemActivity.this, "Insufficient Stock", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Tools.dismissKeyboard(AddItemActivity.this);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Tools.dismissKeyboard(AddItemActivity.this);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
