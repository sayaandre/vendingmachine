package com.example.vendingmachine.Activity;

import android.content.ClipData;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.vendingmachine.Fragment.AboutMeFragment;
import com.example.vendingmachine.Fragment.CartFragment;
import com.example.vendingmachine.Fragment.ExploreFragment;
import com.example.vendingmachine.Models.Realm.ItemMaster;
import com.example.vendingmachine.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmResults;

public class MainActivity extends BaseActivity {

    @BindView(R.id.bottom_navigation) BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Vending Machine");
        ButterKnife.bind(this);
        setDataDummy();
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment = null;
                switch (item.getItemId()) {
                    case R.id.action_sales:
                        fragment = new ExploreFragment();
                        break;
                    case R.id.action_cart:
                        fragment = new CartFragment();
                        break;
                    case R.id.action_me:
                        fragment = new AboutMeFragment();
                        break;
                }
                try {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, fragment);
                    fragmentTransaction.commit();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                return true;
            }
        });
        setFirstFragment();
    }

    public void setFirstFragment()
    {
        Fragment fragment = new ExploreFragment();
        try {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.content_frame, fragment);
            fragmentTransaction.commit();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public void clearDataDummy()
    {
        realm.beginTransaction();
        RealmResults<ItemMaster> itemMasters = realm.where(ItemMaster.class).findAll();
        itemMasters.deleteAllFromRealm();
        realm.commitTransaction();
    }

    public void setDataDummy()
    {
        clearDataDummy();
        realm.beginTransaction();
        ItemMaster itemMaster = new ItemMaster();
        itemMaster.setItemId(1);
        itemMaster.setItemName("Biskuit");
        itemMaster.setItemPrice(6000);
        itemMaster.setItemStok(20);
        itemMaster.setItemImage("https://drive.google.com/uc?id=170vzn87vFmQdoRSlqllCrhp89PxsYEB8");
        realm.copyToRealmOrUpdate(itemMaster);
        itemMaster = new ItemMaster();
        itemMaster.setItemId(2);
        itemMaster.setItemName("Chips");
        itemMaster.setItemPrice(8000);
        itemMaster.setItemStok(30);
        itemMaster.setItemImage("https://drive.google.com/uc?id=12x6OB-NmjMcaIZRNDnkl7ju34vvgGfoo");
        realm.copyToRealmOrUpdate(itemMaster);
        itemMaster = new ItemMaster();
        itemMaster.setItemId(3);
        itemMaster.setItemName("Oreo");
        itemMaster.setItemPrice(10000);
        itemMaster.setItemStok(40);
        itemMaster.setItemImage("https://drive.google.com/uc?id=1zIkqsEEYtFMfmcl5LwKt4OtwUqB6BWGF");
        realm.copyToRealmOrUpdate(itemMaster);
        itemMaster = new ItemMaster();
        itemMaster.setItemId(4);
        itemMaster.setItemName("Tango");
        itemMaster.setItemPrice(12000);
        itemMaster.setItemStok(25);
        itemMaster.setItemImage("https://drive.google.com/uc?id=1K-QfhofpDAPH7vmdFoFDmh971qMw66gm");
        realm.copyToRealmOrUpdate(itemMaster);
        itemMaster = new ItemMaster();
        itemMaster.setItemId(5);
        itemMaster.setItemName("Cokelat");
        itemMaster.setItemPrice(15000);
        itemMaster.setItemStok(35);
        itemMaster.setItemImage("https://drive.google.com/uc?id=1a3PTXArvqlY2drhzdewk0IA0VN9ZzgcU");
        realm.copyToRealmOrUpdate(itemMaster);
        realm.commitTransaction();
    }
}
