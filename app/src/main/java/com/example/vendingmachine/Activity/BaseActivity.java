package com.example.vendingmachine.Activity;

import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;


import io.realm.Realm;


public class BaseActivity extends AppCompatActivity {

    Realm realm;
    ProgressDialog progressDialog;
    boolean isTablet = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
    }

    public void dismissProgressDialog() {
        if(!isFinishing() && progressDialog != null && progressDialog.isShowing()) {
            if(progressDialog.isShowing())
                progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    protected void onDestroy() {
        try {
            if(realm != null && !realm.isClosed())
                realm.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        dismissProgressDialog();
        super.onDestroy();
    }
}
