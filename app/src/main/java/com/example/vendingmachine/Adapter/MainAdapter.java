package com.example.vendingmachine.Adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.vendingmachine.Helper.CircleTransform;
import com.example.vendingmachine.Helper.Tools;
import com.example.vendingmachine.Models.Model.ItemMasterModel;
import com.example.vendingmachine.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder>{

    private final OnItemClickListener listener;
    private List<ItemMasterModel> header;
    private List<ItemMasterModel> headerOri;
    private SparseBooleanArray selectedItems;
    boolean tabletSize = false;
    Context mContext;
    Filter filter;
    private int focusedItem = -1;

    public MainAdapter(Context context, List<ItemMasterModel> header, OnItemClickListener listener) {
        this.header = header;
        this.listener = listener;
        this.header = new ArrayList<ItemMasterModel>(header);
        this.headerOri = new ArrayList<ItemMasterModel>(header);
        selectedItems = new SparseBooleanArray(1);
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.click(header.get(position), listener, position);
        final ItemMasterModel album = header.get(position);
        holder.txtName.setText(Tools.convertMoney(mContext, header.get(position).getItemPrice()));
        holder.txtAcc.setText(header.get(position).getItemName());
        if(album.getItemImage() == null || (album.getItemImage() != null && album.getItemImage().equals(""))) {
            String url = "https://drive.google.com/uc?id=1GUCJBU7EQR5mm_WZ9hZhMOE4r65W0_qf";
            Picasso.with(mContext)
                    .load(url)
                    .centerCrop()
                    .transform(new CircleTransform(50,0))
                    .fit()
                    .into(holder.ivItem);
        } else {
            Picasso.with(mContext)
                    .load(album.getItemImage())
                    .centerCrop()
                    .transform(new CircleTransform(50,0))
                    .fit()
                    .into(holder.ivItem);
        }
    }

    @Override
    public int getItemCount() {
        return header.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtName, txtAcc;
        LinearLayout llReport;
        CardView cardRow;
        ImageView ivItem;

        public ViewHolder(View convertView) {
            super(convertView);
            txtName = convertView.findViewById(R.id.txtItemStock);
            txtAcc = convertView.findViewById(R.id.txtItemName);
            llReport = convertView.findViewById(R.id.llItemMaster);
            cardRow = convertView.findViewById(R.id.card_view);
            ivItem = convertView.findViewById(R.id.ivImageItem);
        }

        public void click(final ItemMasterModel dataModel, final OnItemClickListener listener, final int position){
            cardRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    notifyItemChanged(getAdapterPosition());
                    focusedItem = getLayoutPosition();
                    notifyItemChanged(getAdapterPosition());
                    notifyDataSetChanged();
                    listener.onClick(dataModel, position);
                }
            });
        }
    }

    public void updateData(List<ItemMasterModel> headerList) {
        header.clear();
        header.addAll(headerList);
    }

    public interface OnItemClickListener{
        void onClick(ItemMasterModel item, int position);
    }
}
