package com.example.vendingmachine.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.vendingmachine.Helper.Tools;
import com.example.vendingmachine.Models.Model.CartModel;
import com.example.vendingmachine.Models.Realm.ItemMaster;
import com.example.vendingmachine.R;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {

    private final OnItemClickListener listener;
    private List<CartModel> cartsList;
    Context mContext;
    Realm realm;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView skuid, qty, desc, price, txtInitial, product, txtgivespace, finalprice;
        public ImageButton imgDelete;
        public LinearLayout llCart;
        public ImageView imgItem;
        public RelativeLayout viewBackground, viewForeground;

        public MyViewHolder(View view) {
            super(view);
            skuid = view.findViewById(R.id.txtSKUId);
            product = view.findViewById(R.id.txtProductName);
            txtgivespace = view.findViewById(R.id.txtGiveSpace);
            qty = view.findViewById(R.id.txtQty);
            desc = view.findViewById(R.id.txtPLUDesc);
            price = view.findViewById(R.id.txtPrice);
            imgDelete = view.findViewById(R.id.imgDelete);
            llCart = view.findViewById(R.id.llCart);
            txtInitial = view.findViewById(R.id.txtInitial);
            imgItem = view.findViewById(R.id.imgItem);
            finalprice = view.findViewById(R.id.txtFinalPrice);
            viewBackground = view.findViewById(R.id.llBackground);
            viewForeground = view.findViewById(R.id.llForeground);
        }
        public void click(final CartModel cartModel, final OnItemClickListener listener){
            llCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(cartModel);
                }
            });

            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onDeleteClick(cartModel);
                }
            });
        }
    }

    public CartAdapter(Context context, List<CartModel> cartsList, OnItemClickListener listener) {
        this.cartsList = cartsList;
        this.listener = listener;
        this.mContext = context;
        realm = Realm.getDefaultInstance();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_cart, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        CartModel cart = cartsList.get(position);
        Locale current = mContext.getResources().getConfiguration().locale;
        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance(current);
        DecimalFormat formatter = new DecimalFormat("#,###,###", symbols);
        holder.click(cart, listener);
        String[] namepart = cart.getItemName().split("\\|");
        String desc = "";
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)
                holder.desc.getLayoutParams();
        holder.txtgivespace.setVisibility(View.GONE);
        holder.product.setVisibility(View.VISIBLE);
        holder.product.setText(namepart[0]);
        holder.desc.setText(Html.fromHtml("<b>" + namepart[0] + "</b>"));
        params.weight = 0.45f;
        //holder.desc.setTextSize(mContext.getResources().getDimension(R.dimen.font_normal));
        desc = namepart[0];
        holder.desc.setLayoutParams(params);
        holder.skuid.setVisibility(View.GONE);
        holder.qty.setText(" x " + String.valueOf(cart.getItemQty()));
        holder.price.setText(formatter.format(cart.getItemPrice()));
        holder.txtInitial.setText(desc.length() > 2 ? desc.substring(0,2) : desc);
        double fprice = cart.getItemTotal();
        final ItemMaster itemMaster = realm.where(ItemMaster.class)
                .equalTo("itemId", cart.getItemId()).findFirst();
        holder.finalprice.setText(formatter.format(fprice));
    }

    @Override
    public int getItemCount() {
        return cartsList.size();
    }

    public interface OnItemClickListener{
        void onClick(CartModel item);
        void onDeleteClick(CartModel item);
    }
}
