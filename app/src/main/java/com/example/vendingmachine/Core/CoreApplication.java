package com.example.vendingmachine.Core;

import android.app.Application;
import android.content.Intent;
import android.os.Process;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class CoreApplication extends Application {

    public static CoreApplication app;
    Realm realm;

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        RealmConfiguration realmConfig = new RealmConfiguration.Builder()
                .name("VendingMachine.realm")
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfig);
        app = this;
    }

    public static CoreApplication getInstance() {
        return app;
    }

    public void closeApplication() {
        Process.killProcess(Process.myPid());
    }
}
