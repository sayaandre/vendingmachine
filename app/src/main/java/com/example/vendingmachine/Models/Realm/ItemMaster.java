package com.example.vendingmachine.Models.Realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ItemMaster extends RealmObject {
    @PrimaryKey
    private int itemId;
    private String itemName;
    private double itemPrice;
    private int itemStok;
    private String itemImage;

    public String getItemImage() {
        return itemImage;
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public int getItemStok() {
        return itemStok;
    }

    public void setItemStok(int itemStok) {
        this.itemStok = itemStok;
    }
}
