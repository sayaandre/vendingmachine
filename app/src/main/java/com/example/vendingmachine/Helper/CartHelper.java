package com.example.vendingmachine.Helper;

import android.content.Context;

import com.example.vendingmachine.Models.Model.CartModel;
import com.example.vendingmachine.Models.Realm.Cart;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class CartHelper {

    private Realm realm;

    public CartHelper(Context context) {
        realm = Realm.getDefaultInstance();
    }

    public void addCart(int id, String name, int qty, double price, String image)
    {
        realm.beginTransaction();
        RealmResults<Cart> carts = realm.where(Cart.class).equalTo("itemId", id).findAll();
        carts.deleteAllFromRealm();
        realm.commitTransaction();
        realm.beginTransaction();
        Cart cart = realm.where(Cart.class).equalTo("itemId", id).findFirst();
        if(cart == null)
        {
            cart = new Cart();
            cart.setRowNo(Tools.dapatkanId());
            cart.setItemId(id);
            cart.setItemName(name);
            cart.setItemPrice(price);
            cart.setItemImage(image);
        }
        cart.setItemQty(qty);
        cart.setItemTotal((price * cart.getItemQty()));
        realm.copyToRealmOrUpdate(cart);
        realm.commitTransaction();
    }

    public List<CartModel> getCart()
    {
        List<CartModel> cartModels = new ArrayList<>();
        RealmResults<Cart> carts = realm.where(Cart.class)
                .findAll()
                .sort("rowNo", Sort.DESCENDING);
        for(int i = 0 ; i < carts.size() ; i++)
        {
            CartModel cartModel = new CartModel(carts.get(i).getRowNo(),
                    carts.get(i).getItemId(), carts.get(i).getItemName(), carts.get(i).getItemPrice(),
                    carts.get(i).getItemQty(), carts.get(i).getItemTotal(), carts.get(i).getItemImage());
            cartModels.add(cartModel);
        }
        return cartModels;
    }

    public int getTotalInCart() {
        realm = Realm.getDefaultInstance();
        int total = realm.where(Cart.class)
                .sum("itemTotal").intValue();
        return total;
    }
}
