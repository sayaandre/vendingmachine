package com.example.vendingmachine.Helper;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.inputmethod.InputMethodManager;

import com.example.vendingmachine.R;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Tools {

    public static double processMoney(String money) {
        return Double.parseDouble(money.replace(",", "").replace(".", ""));
    }

    public static String convertMoney(Context mContext, double amount)
    {
        Locale current = mContext.getResources().getConfiguration().locale;
        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance(current);
        DecimalFormat formatter = new DecimalFormat("#,###,###", symbols);
        return formatter.format(amount);
    }

    public static void dismissKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (null != activity.getCurrentFocus())
            imm.hideSoftInputFromWindow(activity.getCurrentFocus()
                    .getApplicationWindowToken(), 0);
    }

    public static long dapatkanId() {
        long dateTime = 0;
        Calendar c = Calendar.getInstance();
        System.out.println("Current time =&gt; " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("HHmmssSSS");
        dateTime = Long.parseLong(df.format(c.getTime()));

        return dateTime;
    }

    public static Date getDateFromStringFull(String value) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        // formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            if (value == null || value.equals(""))
                return new Date();
            return formatter.parse(value);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return new Date();
    }

    public static Bitmap resizeBitmap(Bitmap getBitmap, int maxSize) {
        int width = getBitmap.getWidth();
        int height = getBitmap.getHeight();
        double x;

        if (width >= height && width > maxSize) {
            x = width / height;
            width = maxSize;
            height = (int) (maxSize / x);
        } else if (height >= width && height > maxSize) {
            x = height / width;
            height = maxSize;
            width = (int) (maxSize / x);
        }
        return Bitmap.createScaledBitmap(getBitmap, width, height, false);
    }

    public static void onCreateSwipeToRefresh(SwipeRefreshLayout layout) {
        layout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    public static void createAlertDialog(Context context, String message)
    {
        AlertDialog.Builder kotakBuilder = new AlertDialog.Builder(context,
                R.style.AppCompatAlertDialogStyle);
        kotakBuilder.setIcon(context.getResources().getDrawable(R.drawable.ic_shortcut_baseline_report_problem_black_48));
        kotakBuilder.setTitle("Notice");
        kotakBuilder.setMessage(message);
        kotakBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = kotakBuilder.create();
        dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.rounded_white));
        dialog.show();
    }
}
