package com.example.vendingmachine.Helper;

import com.example.vendingmachine.Models.Model.ItemMasterModel;
import com.example.vendingmachine.Models.Realm.ItemMaster;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class ItemMasterHelper {
    private Realm realm;

    public ItemMasterHelper() {
        realm = Realm.getDefaultInstance();
    }

    public List<ItemMasterModel> getItemMaster()
    {
        List<ItemMasterModel> itemMasterModelList = new ArrayList<>();
        RealmResults<ItemMaster> itemMasters = realm.where(ItemMaster.class).findAll();
        for(int i = 0 ; i < itemMasters.size() ; i++)
        {
            ItemMasterModel itemMasterModel = new ItemMasterModel(itemMasters.get(i).getItemId(), itemMasters.get(i).getItemName(),
                    itemMasters.get(i).getItemPrice(), itemMasters.get(i).getItemStok(), itemMasters.get(i).getItemImage());
            itemMasterModelList.add(itemMasterModel);
        }
        return itemMasterModelList;
    }

}
